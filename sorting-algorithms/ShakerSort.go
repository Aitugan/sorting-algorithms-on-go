package SortingAlgorithms

func ShakerSort (array []int) []int {
	swapped := true
	start := 0
	end := len(array)-1
	for swapped {
		swapped = false
		for i := start; i < end; i++ {
			if array[i] > array[i + 1] {
				array[i], array[i + 1] = array[i + 1], array[i]
				swapped = true
			}
		}
		if !swapped {
			break
		}

		swapped = false
		end--

		for i := end-1; i > start-1; i-- {
			if array[i] > array[i + 1] {
				array[i], array[i + 1] = array[i + 1], array[i]
				swapped = true				
			}
		}
		start++
	}

	return array
}
