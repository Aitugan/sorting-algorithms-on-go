package SortingAlgorithms

import (
	"math/rand"
)

func QuickSort(array []int) []int {
	if len(array) <= 1 {
		return array
	}

	pivot := array[rand.Intn(len(array))]

	lowPart := make([]int, 0, len(array))
	highPart := make([]int, 0, len(array))
	middlePart := make([]int, 0, len(array))

	for i := range array {
		if array[i] < pivot {
			lowPart = append(lowPart,array[i])	
		} else
		if array[i] > pivot {
			highPart = append(highPart,array[i])
		} else {
			middlePart = append(middlePart,array[i])
		}
	}

	lowPart = QuickSort(lowPart)
	highPart = QuickSort(highPart)

	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)
	return lowPart
}
