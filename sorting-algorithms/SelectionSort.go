package SortingAlgorithms

func SelectionSort (array []int) []int {
	for i := 0; i < len(array); i++ {
		min := i
		for j := i+1; j < len(array); j++ {
			if array[min] > array[j] {
				min = j
			}
		}
		array[i], array[min] = array[min], array[i]
	}
	return array
}
