package main

import (
	"fmt"
	sort "gitlab.com/Aitugan/sorting-algorithms-on-go/sorting-algorithms"
)

func main() {
	array := []int{10,9,8,7,6,5,4,3,2,1}
	fmt.Println("Original array is: ",array)
	fmt.Println("Bubble Sort: ",sort.BubbleSort(array))
	fmt.Println("Insertion Sort: ",sort.InsertionSort(array))
	fmt.Println("Merge Sort: ",sort.MergeSort(array))
	fmt.Println("Quick Sort: ",sort.QuickSort(array))
	fmt.Println("Selection Sort: ",sort.SelectionSort(array))
	fmt.Println("Shaker Sort: ",sort.ShakerSort(array))

}